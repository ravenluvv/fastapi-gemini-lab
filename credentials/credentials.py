import os
import gspread
from google.oauth2.service_account import Credentials


SCOPES = ['https://spreadsheets.google.com/feeds',
          'https://www.googleapis.com/auth/drive']


# get path of credentials.json

__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))


credentials = Credentials.from_service_account_file(
    os.path.join(__location__, 'credentials.json'), scopes=SCOPES)
gc = gspread.authorize(credentials)
