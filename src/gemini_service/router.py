from fastapi import APIRouter, UploadFile, status
from src.gemini_service.schemas import UserInfoResponse
from src.gemini_service import gemini_model_service, google_sheet_service
router = APIRouter()


@router.post("/upload_files", status_code=status.HTTP_201_CREATED, response_model=list[UserInfoResponse])
async def create_upload_files(files: list[UploadFile]):
    response = []
    for file in files:
        user_info = await gemini_model_service.extract_info(file.file)
        user_schema = UserInfoResponse(**user_info)
        user_info = user_schema.model_dump()

        await google_sheet_service.append_new_row(user_info)
        response.append(user_schema)
    return response
