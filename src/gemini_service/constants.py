GOOGLE_SHEET = ""
PROMT_TEMPLATE = """
From the provided text,  extract the following information as key-value:

- Name
- Title
- Date of Birth (format: YYYY-MM-DD)
- Gender
- Phone
- Email
- Address
- Education
- Work Experience
- Technology Skills
- Language Skills
- Process Skills
- Soft Skills

Your output should be a JSON object with the following specifications:
- Use snake_case and lowercase for all keys.
- Keep values in their original format.
- Represent each field as a string. Avoid nested objects or lists.
- If a field is not found, represent it with an empty string ('').
- Do not use markdown in the output.
"""

SHEET_TITLE = "Candidate Information"
SPREADSHEET_ID = "1iNPpvEK4pXQA9XXIjZNMFa3dKWiLbKZahwv5HKsBqhM"
