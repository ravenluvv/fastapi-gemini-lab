import os

from dotenv import load_dotenv
import google.generativeai as genai


load_dotenv()
genai.configure(api_key=os.getenv('GOOGLE_API_KEY'))


gemini_pro = genai.GenerativeModel('gemini-pro')

gemini_pro_vision = genai.GenerativeModel('gemini-pro-vision')
