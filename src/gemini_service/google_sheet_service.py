from __future__ import print_function
import os.path
from gspread.exceptions import WorksheetNotFound
from gspread.spreadsheet import Spreadsheet
from gspread.worksheet import Worksheet

from credentials import credentials
import pandas as pd
import google.generativeai as genai


from dotenv import load_dotenv
from src.gemini_service import constants
from src.gemini_service import schemas

from src.gemini_service.schemas import UserInfoResponse


load_dotenv()

genai.configure(api_key=os.getenv("GOOGLE_API_KEY"))


# get path of credentials.json

SPREADSHEET_ID = os.getenv("SPREADSHEET_ID")


def get_spreadsheet(spreadsheet_id) -> Spreadsheet:
    try:
        spreadsheet = credentials.gc.open_by_key(spreadsheet_id)
    except Exception:
        spreadsheet = credentials.gc.create(title=constants.SHEET_TITLE)

    return spreadsheet


def get_worksheet(spreadsheet: Spreadsheet) -> Worksheet:

    try:
        worksheet = spreadsheet.worksheet(constants.SHEET_TITLE)
    except WorksheetNotFound:
        worksheet = spreadsheet.add_worksheet(
            title=constants.SHEET_TITLE, rows=100, cols=20)
        worksheet.resize(1)
        column_names = [e.value for e in schemas.Column]

        worksheet.append_row(column_names)

    return worksheet


def append_new_row(info: dict):
    spreadsheet = get_spreadsheet(SPREADSHEET_ID)
    worksheet = get_worksheet(spreadsheet)

    row = list(info.values())
    worksheet.append_row(row)
    return True
