from pydantic import BaseModel, Field, EmailStr
from src.models import CustomModel
from datetime import datetime
from enum import Enum


class Genders(Enum):
    male = "Male"
    female = "Female"


class UserInfoResponse(CustomModel):
    name: str = Field(...)
    title: str  = Field(default="")
    date_of_birth: str  = Field(
        default="")
    gender: Genders  = Field(default="")
    phone: str = Field(min_length=10, max_length=10, default="")
    email: EmailStr  = Field(default="")
    address: str  = Field(default="")
    education: str  = Field(default="")
    work_experience: str = Field(default="")
    technologies_skills: str = Field(default="")
    language_skills: str = Field(default="")
    process_skills: str = Field(default="")
    soft_skills: str = Field(default="")
    class Config:  
            use_enum_values = True

class Genders(Enum):
    male = "Male"
    female = "Female"


class Column(Enum):
    name = "Name"
    title = "Title"
    date_of_birth = "Date of Birth"
    gender = "Gender"
    phone = "Phone"
    email = "Email"
    address = "Address"
    education = "Education"
    work_experience = "Work Experience"
    technologies_skills = "Technologies Skills"
    process_skills = "Process Skills"
    soft_skills = "Soft skills"
