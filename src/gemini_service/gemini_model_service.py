import datetime
import json
from typing import BinaryIO

from fastapi import UploadFile

from src.gemini_service import constants, utils, gemini_models


async def run_prompt(prompt_text, context_text) -> str:
    model = gemini_models.gemini_pro
    responses = model.generate_content(
        f"{prompt_text}\n{context_text}", stream=False)
    return responses.candidates[0].content.parts[0].text


async def extract_info(file: BinaryIO) -> dict:
    text = utils.get_pdf_text(file)
    response = await run_prompt(constants.PROMT_TEMPLATE, text)
    response = utils.remove_markdown(response)
    json_response = json.loads(response)
    processed_info = preprocess_info(json_response)

    return processed_info
# this function that preprocesses the info because gemini model is not perfect


def preprocess_info(info: dict) -> dict:
    info["gender"] = info["gender"].capitalize()
    return info
