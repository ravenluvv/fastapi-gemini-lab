import json
import re
from typing import BinaryIO

from PyPDF2 import PdfReader
from fastapi import UploadFile


def get_pdf_text(pdf_doc: BinaryIO) -> str:
    text = ""
    pdf_reader = PdfReader(pdf_doc)
    for page in pdf_reader.pages:
        text += page.extract_text()
    return text


def remove_markdown(text: str) -> str:

    return text.strip("```json")  # Dont know why it contains ```json```
