import logging
import random
import string
import requests
from PIL import Image
from io import BytesIO

logger = logging.getLogger(__name__)
ALPHA_NUM = string.ascii_letters + string.digits


def generate_random_alphanum(length: int = 20) -> str:
    return "".join(random.choices(ALPHA_NUM, k=length))

def download_image(url: str) -> None:
    response = requests.get(url)
    img = Image.open(BytesIO(response.content))
    return img

def read_image(path: str) -> None:
    img = Image.open(path)
    return img
